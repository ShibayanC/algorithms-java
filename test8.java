import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class test8{
	public static String REGEX = "dog";
	public static String INPUT = "The dog says meow. " +
					"All dogs says meow.";
	public static String REPLACE = "cat";
	public static void main(String[] args){
		Pattern p = Pattern.compile(REGEX);
		Matcher m = p.matcher(INPUT);
		INPUT = m.replaceAll(REPLACE);
		System.out.println(INPUT);
	}
}

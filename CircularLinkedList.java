import java.util.*;

class Node{
	Integer data;
	Node next;
	public Node(int val){
		data = val;
		next = null;
	}
}

public class CircularLinkedList{
	private static Node head = null;

	public static void insert(int val){
		Node newNode = new Node(val);
		if (head == null){
			head = newNode;
			newNode.next = head;
		}
		else{
			newNode.next = head;
			head = newNode;
		}
	}

	public static void display(int len){
		Node ptr = head.next;
		if (len == 1)
			System.out.print(head.data+"->null\n");
		else{
			while(ptr.next != head){
				System.out.print(ptr.data+"->");
				ptr = ptr.next;
			}
			System.out.print(head.data+"->null\n");
		}
	}

	public static void main(String[] args){
		CircularLinkedList c = new CircularLinkedList();

		int size = 0;
		int [] arr = {12, 21, 34, 56, 64, 74, 89, 90};
		for(int i:arr){
			size ++;
			c.insert(i);	
			c.display(size);
		}	
	}
}

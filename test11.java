/* This program shows the
 * inheritance feature in JAVA */
import java.util.Scanner;

class Calculator{
	Integer z;
	int add(int x, int y){
		z = x + y;
		return z;
	}

	int sub(int x, int y){
		z = x - y;
		return z;
	}

	int mult(int x, int y){
		z = x * y;
		return z;
	}
}

public class test11 extends Calculator{
	
	public static void main(String args[]){
		Scanner in = new Scanner(System.in);
		test11 t = new test11();
		int x, y;
		System.out.print("Enter the first num: ");
		x = in.nextInt();
		System.out.print("Enter the second num: ");
		y = in.nextInt();
		System.out.println("Add: "+t.add(x, y)+ " Sub: "+t.sub(x, y)+ "Prod: "+t.mult(x, y)+
										t.mod(x, y));
	}

	public int mod(int x, int y)
	{
		Integer z;
		z = x % y;
		return z;
	}
}

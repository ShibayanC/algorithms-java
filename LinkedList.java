import java.util.Scanner;

class Node{
	Node next;
	int num;
	public Node(int val){
		num = val;
		next = null;
		}
}

public class LinkedList{
	private static Node head1 = null;
	private static Node head2 = null;

	public static void insert(int val){
		Node newNode = new Node(val);
		if (head1 == null){
			head1 = newNode;
		}
		else{
			newNode.next = head1;
			head1 = newNode;
		}
	}

	public static void insertatend(int val){
		Node newNode = new Node(val);
		if (head2 == null){
			head2 = newNode;
		}
		else{
			Node cur = head2;
			while(head2 != null){
				if (head2.next == null){
					cur = head2;
				}
				head2 = head2.next;
			}
			cur.next = newNode;
		}
	}

	public static void delete(int val){
		Integer flag = 0;
		if (head1 == null)
			return;
		else if (head1.num == val){
			flag = 1;
			head1.next = head1.next.next;
		}
		else{
			Node cur = head1;
			while(cur.next != null){
				if (cur.next.num == val){
					cur.next = cur.next.next;
					flag = 1;
				}
			cur = cur.next;
			}
		}
		if (flag == 0)
			System.out.println("Number not present");
	}

	public static void display(){
		Node cur = head1;
		while(cur != null){
			System.out.print(cur.num+"->");
			cur = cur.next;
		}
		System.out.print("null  \n ");
	}

	public static void displayrev(){
		Node cur = head2;
		while(cur != null){
			System.out.print(cur.num+"->");
			cur = cur.next;
		}
		System.out.print("null\n");
	}

	public static void main(String[] args){
		LinkedList l = new LinkedList();
		int [] arr = {12, 23, 45, 56, 78, 90};
		for(int i: arr){
			System.out.print(i+" ");
			l.insert(i);
			//l.insertatend(i);
			l.display();
			//l.displayrev();
		}
		l.delete(45);
		l.display();		
		System.out.println("\n");	
	}
}	

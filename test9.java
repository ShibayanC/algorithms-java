import java.util.*;

class myClass{
	int x, val = 0;
	static int Val = 15;
	myClass(int i){
		x = i;
	}
	int sum(int y){
		val = y + Val;
		return val;
	}
}

public class test9{
	public static void main(String[] args){
	myClass t1 = new myClass(10);
	myClass t2 = new myClass(20);
	System.out.println(t1.x + " " + t2.x);
	System.out.println("First Val: "+ t1.sum(100));
	System.out.println("Second Val: "+ t2.sum(200));
	}
}

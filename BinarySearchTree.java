import java.util.*;

class Tree{
	Integer data;
	Tree left;
	Tree right;
	public Tree(int val){
		data = val;
		left = null;
		right = null;
	}
}

public class BinarySearchTree{
	private static Tree root = null;

	public static Tree newTreeNode(int val){
		Tree newNode = new Tree(val);
		return newNode;
	}

	public static Tree insertTreeNode(Tree node, int val){
		Tree retNode;
		if (node == null){
			retNode = newTreeNode(val);
			return retNode;
		}
		else{
			if (val <= node.data)
				node.left = insertTreeNode(node.left, val);
			else
				node.right = insertTreeNode(node.right, val);
		}
		return node;
	}

	public static void printInOrder(Tree node){
		if (node == null)
			return;
		else{
			printInOrder(node.left);
			System.out.print(node.data+" ");
			printInOrder(node.right);
		}
	}

	public static void printPreOrder(Tree node){
		if (node == null)
			return;
		else{
			System.out.print(node.data+" ");
			printPreOrder(node.left);
			printPreOrder(node.right);
		}
	}

	public static void printPostOrder(Tree node){
		if (node == null)
			return;
		else{
			printPostOrder(node.left);
			printPostOrder(node.right);
			System.out.print(node.data+" ");
		}
	}

	public static Tree minTree(Tree node){
		if (node == null)
			return null;
		while(node.left != null){
			node = node.left;
		}
		return node;
	}

	public static Tree maxTree(Tree node){
		if (node == null)
			return null;
		while(node.right != null){
			node = node.right;
		}
		return node;
	}
	
	public static int treeHeight(Tree node){
		if (node == null)
			return 0;
		int leftHt = treeHeight(node.left) + 1;
		int rightHt = treeHeight(node.right) + 1;
		return (leftHt > rightHt ? leftHt + 1 : rightHt + 1);
	}

	public static int treeSize(Tree node){
		if (node == null)
			return 0;
		else{
			int leftTree = treeSize(node.left) + 1;
			int rightTree = treeSize(node.right) + 1;
			return (leftTree + rightTree + 1);
		}
	}

	public static Tree search(Tree node, int val){
		if (node == null)
			return null;
		else if (node.data == val)
			return node;
		else{
			if (val < node.data)
				search(node.left, val);
			else
				search(node.right, val);
		}
		return node;
	}
	/*
	public static boolean isBalanced(Tree node){
		int leftTree = treeSize(node.left);
		int rightTree = treeSize(node.right);
		if ((leftTree - rightTree) <= 1)
			return true;
		else
			return false;
	}*/

	public static void main(String[] args){
		int[] arr = {56, 12, 67, 78, 98, 43, 4, 28, 39, 88, 19};
		BinarySearchTree bt = new BinarySearchTree();
		Tree root, min, max, loc;
		root = bt.newTreeNode(arr[0]);
		for(int i = 1; i < arr.length; i++){
			bt.insertTreeNode(root, arr[i]);
		}

		System.out.println("Display Tree InOrder: ");
		bt.printInOrder(root);
		System.out.println(" ");
		System.out.println("Display Tree PreOrder: ");
		bt.printPreOrder(root);
		System.out.println(" ");
		System.out.println("Display Tree PostOrder: ");
		bt.printPostOrder(root);
		System.out.println(" ");
		min = bt.minTree(root);
		max = bt.maxTree(root);
		System.out.println("Min : Max -> " +min.data+":"+max.data);
		int treeHt, treeSz;
		treeHt = bt.treeHeight(root);
		treeSz = bt.treeSize(root);
		System.out.println("Tree Height: "+treeHt+" Tree Size: "+treeSz);
		loc = bt.search(root, 98);
		System.out.println("Location of 98 is: "+loc);
		loc = bt.search(root, 96);
		System.out.println("Location of 96 is: "+loc);
		// boolean cond = bt.isBalanced();
		// System.out.println("Condition: "+cond);
		System.out.print("\n");
	}
}


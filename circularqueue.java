import java.util.*;

class Node{
	Integer data;
	Node next;
	public Node(int val){
		data = val;
		next = null;
	}
}

public class circularqueue{
	private static Node head = null;
	private static Node tail = null;
	private static Size = 0;

	public static void enqueue(int val){
		Node newNode = new Node(val);
		if (head == null){
			head = newNode;
			tail = newNode;
		}
		else{
			tail.next = newNode;
			tail = newNode;
		}
	}

	public static void dequeue(){
	}

	public static void display(){
	}

	public static void size(){
		System.out.println("Size of queue: "+Size);
	}

	public static void main(String [] args){
		circularqueue cq = new circularqueue();
		int[] arr = {12, 34, 43, 56, 69, 87, 98};
		for(int i:arr){
			cq.enqueue(i);
			cq.display();
			cq.size();
		}

		cq.dequeue();
		cq.display();
		cq.size();

		cq.dequeue();
		cq.display();
		cq.size();
	}
}

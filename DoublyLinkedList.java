import java.util.*;

class Node{
	Integer data;
	Node next;
	Node front;
	public Node(int val){
		data = val;
		next = null;
		front = null;
	}
}
	
public class DoublyLinkedList{
	private static Node head = null;

	public static void insert(int val){
		Node newNode = new Node(val);
		if (head == null)
			head = newNode;
		else{
			head.front = newNode;
			newNode.next = head;
			head = newNode;
		}
	}

	public static void display(){
		Node cur = head;
		Node ptr = null;
		System.out.print("Display: ");
		while(cur != null){
			System.out.print(cur.data+"->");
			if (cur.next == null)
				ptr = cur;
			cur = cur.next;
		}
		System.out.print("null  ");

		System.out.print("Display rev: ");
		while(ptr != null){
			System.out.print(ptr.data+"->");
			ptr = ptr.front;
		}
		System.out.print("null\n");
	}

	public static void delete(int val){
		Integer flag = 0;
		if (head == null)
			return;
		else if (head.data == val){
			flag = 1;
			head.next.front = null;
			head = head.next;
		}
		else{
			Node ptr = head;
			while(ptr.next != null){
				if (ptr.next.data == val){
					flag = 1;
					ptr.next.next.front = ptr;
					ptr.next = ptr.next.next;
				}
				ptr = ptr.next;
			}
		}
		if (flag == 1)
			System.out.println("Number not present !");
	}

	public static void main(String []args){
		DoublyLinkedList d = new DoublyLinkedList();
		int[] arr = {12, 34, 67, 87, 45, 90, 58, 77};
		for(int i:arr)
		{
			d.insert(i);
			d.display();
		}
		d.delete(87);
		d.delete(46);
		System.out.println("LL after delete: ");
		d.display();
		System.out.println("\n");
	}
}		

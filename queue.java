import java.util.*;

class Node{
	Integer data;
	Node next;
	public Node(int val){
		data = val;
		next = null;
	}
}

public class queue{
	private static Node head = null;
	private static Node tail = null;
	private static int Size = 0;

	public static void enqueue(int val){
		Node newNode = new Node(val);
		if (head == null){
			head = tail = newNode;
			Size ++;
		}
		else{
			tail.next = newNode;
			tail = newNode;
			Size ++;
		}		
	}

	public static void dequeue(){
		if (head == null)
			System.out.println("Underflow !");
		else{
			System.out.println("Dequeuing: "+head.data);
			head = head.next;
			Size --;
		}
	}

	public static void size(){
		System.out.println("Size of Queue: "+Size);
	}

	public static void display(){
		Node ptr = head;
		while(ptr != null){
			System.out.print(ptr.data+"->");
			ptr = ptr.next;
		}
		System.out.print("null\n");
	}

	public static void main(String[] args){
		queue q = new queue();
		int [] arr = {12, 23, 34, 56, 78, 90};
		for (int i:arr)
		{
			q.enqueue(i);
			q.display();
			q.size();
		}
		q.dequeue();
		q.size();

		q.dequeue();
		q.size();
	}
}

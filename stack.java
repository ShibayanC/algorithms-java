import java.util.*;

class Node{
	Integer data;
	Node next;
	public Node(int val){
		data = val;
		next = null;
	}
}

public class stack{
	private static Node top = null;

	public static void push(int val){
		Node newNode = new Node(val);
		if (top == null){
			top = newNode;
		}
		else{
			newNode.next = top;
			top = newNode;
		}
	}

	public static void display(){
		Node ptr = top;
		while(ptr != null){
			System.out.print(ptr.data+"->");
			ptr = ptr.next;
		}
		System.out.print("null\n");
	}

	public static void pop(){
		if (top == null)
			System.out.println("Underflow !");
		else{
			System.out.println("Popping out: "+top.data);
			top = top.next;
		}
	}

	public static void peek(){
		System.out.println("Peek: "+top.data);
	}

	public static void main(String [] args){
		stack s = new stack();
		int [] arr = {12, 23, 35, 45, 67, 89, 90};
		for(int i:arr){
			s.push(i);
			s.display();
		}
		s.peek();
		s.pop();
		s.display();

		s.peek();
		s.pop();
		s.display();

		s.peek();
	}
}

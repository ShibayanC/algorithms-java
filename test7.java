public class test7{
	public static void main(String[] args){
		int[] myArray = {12, 34, 24, 78, 69, 90};
		Integer max = 0, min = 0, total = 0;
		for(int i = 0; i < myArray.length; i ++)
		{
			total = total + myArray[i];
			if (max <= myArray[i])
				max = myArray[i];
			else if (min == 0)
				min = myArray[i];
			else
			{
				if (min > myArray[i])
					min = myArray[i];
			}
		}
		System.out.println("Total: "+total);
		System.out.println("Min: "+min);
		System.out.println("Max: "+max);
		printArray(myArray);
		System.out.print("\n");
		reverse(myArray);
		System.out.print("\n");
	}

	public static void printArray(int []array){
		for(int i = 0; i < array.length; i ++)
			System.out.print(array[i]+", ");
	}

	public static void reverse(int []array){
		// System.out.println("I am here");
		int k = array.length;
		k --;
		while(k >= 0)
		{
			System.out.print(array[k]+", ");
			k --;
		}
	}
}	
